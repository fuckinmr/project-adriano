#!/usr/bin/python3
##############################################################################
# project_adriano - Downloads all of Mike Adriano's vids in bulk
#
# Copyright (C) 2023 mrfuckin
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##############################################################################

import requests, re
from bs4 import BeautifulSoup
import os
import shutil
import json
import time
import subprocess
import shlex
from zipfile import ZipFile
from pySmartDL import SmartDL
from pathlib import Path
from tqdm import tqdm


def getlibs():
    print("Initializing Resources")
    if not os.path.exists("libs"):
        os.mkdir("libs")
    if not os.path.exists("libs/youtube-dl.exe"):
        print("Downloading youtube-dl")
        url = "https://youtube-dl.org/downloads/latest/youtube-dl.exe"
        dl = requests.get(url, stream=True, allow_redirects=True)
        with open("libs/" + url.split("/")[-1], "wb") as fout:
            for chunk in dl.iter_content(chunk_size=1024):
                fout.write(chunk)

    if not os.path.exists("libs/aria2c.exe"):
        print("Downloading aria2c")
        url = "https://github.com/aria2/aria2/releases/download/release-1.36.0/aria2-1.36.0-win-64bit-build1.zip"
        dl = requests.get(url, stream=True, allow_redirects=True)
        with open("libs/" + url.split("/")[-1], "wb") as fout:
            for chunk in dl.iter_content(chunk_size=1024):
                fout.write(chunk)
        with ZipFile("libs/aria2-1.36.0-win-64bit-build1.zip") as dl:
            dl.extract("aria2-1.36.0-win-64bit-build1/aria2c.exe", path="libs")
        dl.close()
        os.rename("libs/aria2-1.36.0-win-64bit-build1/aria2c.exe", "libs/aria2c.exe")
        os.rmdir("libs/aria2-1.36.0-win-64bit-build1")
        os.remove("libs/aria2-1.36.0-win-64bit-build1.zip")

    if not os.path.exists("libs/ffprobe.exe"):
        print("Downloading ffprobe")
        url = "https://github.com/ffbinaries/ffbinaries-prebuilt/releases/download/v4.4.1/ffprobe-4.4.1-win-64.zip"
        dl = requests.get(url, stream=True, allow_redirects=True)
        with open("libs/" + url.split("/")[-1], "wb") as fout:
            for chunk in dl.iter_content(chunk_size=1024):
                fout.write(chunk)
        with ZipFile("libs/ffprobe-4.4.1-win-64.zip") as dl:
            dl.extract("ffprobe.exe", path="libs")
        dl.close()
        os.remove("libs/ffprobe-4.4.1-win-64.zip")


getlibs()


print(
    "1 - Analonly  2 - Trueanal  3 - Allanal  4 - Nympho  5 - Swallowed  6 - Bangbros  7 - Newsensations  8 - Pervcity  9 - Evilangel  10 - Dirtyauditions"
)
while True:
    choice = input("Enter Choice: ")
    print("")
    if choice == "1":
        site = "analonly"
        break
    elif choice == "2":
        site = "trueanal"
        break
    elif choice == "3":
        site = "allanal"
        break
    elif choice == "4":
        site = "nympho"
        break
    elif choice == "5":
        site = "swallowed"
        break
    elif choice == "6":
        site = "bangbros"
        break
    elif choice == "7":
        site = "newsensations"
        break
    elif choice == "8":
        site = "pervcity"
        break
    elif choice == "9":
        site = "evilangel"
        break
    elif choice == "10":
        site = "dirtyauditions"
        break
    else:
        print("Invalid Choice!, Try Again")


print("method")
print("1 - by pages  2 - by model  3 - Siterip")
while True:
    choice = input("Enter Choice: ")
    print("")
    if choice == "1":
        method = "pages"
        break
    elif choice == "2":
        method = "models"
        model = input("Enter Model Name: ")
        break
    if choice == "3":
        method = "siterip"
        break
    else:
        print("Invalid Choice!, Try Again")


def getquality(site):
    print("Select Quality")
    if (
        site == "analonly"
        or site == "trueanal"
        or site == "allanal"
        or site == "dirtyauditions"
        or site == "nympho"
        or site == "swallowed"
    ):
        print("1 - 1080p [High Bitrate]   2 - 1080p   3 - 720p   4 - 360p")
        while True:
            choice = input("Enter Choice: ")
            print("")
            if choice == "1":
                quality = "orig"
                break
            elif choice == "2":
                quality = "hq"
                break
            elif choice == "3":
                quality = "stream"
                break
            elif choice == "4":
                quality = "mobile"
                break
            else:
                print("Invalid Choice!, Try Again")

    elif site == "bangbros":
        print("1 - 2160p   2 - 1080p   3 - 720p   4 - 480p")
        while True:
            choice = input("Enter Choice: ")
            print("")
            if choice == "1":
                quality = "2160p"
                break
            elif choice == "2":
                quality = "1080p"
                break
            elif choice == "3":
                quality = "720p"
                break
            elif choice == "4":
                quality = "480p"
                break
            else:
                print("Invalid Choice!, Try Again")

    elif site == "newsensations":
        print("1 - 1080p   2 - 720p   3 - 480p")
        while True:
            choice = input("Enter Choice: ")
            print("")
            if choice == "1":
                quality = "1920x1080MP4_12000"
                break
            elif choice == "2":
                quality = "1280x720MP4_6000"
                break
            elif choice == "3":
                quality = "853MP4_12000"
                break
            else:
                print("Invalid Choice!, Try Again")

    elif site == "pervcity":
        print("1 - 2160p   2 - 1080p   3 - 720p   4 - 480p")
        while True:
            choice = input("Enter Choice: ")
            print("")
            if choice == "1":
                quality = "2160p"
                break
            elif choice == "2":
                quality = "mp412000"
                break
            elif choice == "3":
                quality = "mp48000"
                break
            elif choice == "4":
                quality = "mp42000"
                break
            else:
                print("Invalid Choice!, Try Again")

    elif site == "evilangel":
        print("1 - 1080p   2 - 720p   3 - 576p  4 - 480p  5 - 360p  6 - 288p  7 - 160p")
        while True:
            choice = input("Enter Choice: ")
            print("")
            if choice == "1":
                quality = "1080p"
                break
            elif choice == "2":
                quality = "720p"
                break
            elif choice == "3":
                quality = "576p"
                break
            elif choice == "4":
                quality = "480p"
                break
            elif choice == "5":
                quality = "360p"
                break
            elif choice == "6":
                quality = "288p"
                break
            elif choice == "7":
                quality = "160p"
                break
            else:
                print("Invalid Choice!, Try Again")

    return quality


quality = getquality(site)


def getcookies(site):
    if not os.path.exists("cookies"):
        os.mkdir("cookies")
    cfile = f"cookies/" + site + ".txt"
    if not Path.exists(Path(cfile)):
        print("cookies file missing:", site + ".txt")
        exit(1)
    cookies = {}
    with open(cfile, "r") as fp:
        for line in fp:
            if not re.match(r"^\#", line):
                lineFields = re.findall(r"[^\s]+", line)
                try:
                    cookies[lineFields[5]] = lineFields[6]
                except Exception as e:
                    pass
    return cookies, cfile


cookies, cfile = getcookies(site)
headers = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64)"}

print("Options")
print("1 - Download  2 - Print  3 - Print in txt file")
while True:
    choice = input("Enter Choice: ")
    print("")
    if choice == "1":
        want = "download"
        break
    elif choice == "2":
        want = "print"
        break
    elif choice == "3":
        want = "fprint"
        if not os.path.isdir(f"links/"):
            try:
                os.makedirs(f"links/")
            except OSError as error:
                print(error)
        f = open(f"links/" + site + " links.txt", "w+")
        break
    else:
        print("Invalid Choice!, Try Again")

if want == "download":
    print("Download Location")
    print("1 - Default(downloads in root dir)  2 - Custom dir")
    while True:
        choice = input("Enter Choice: ")
        print("")
        if choice == "1":
            dloc = f"downloads/"
            break
        elif choice == "2":
            dloc = input("Enter Download Location: ")
            break
        else:
            print("Invalid Choice!, Try Again")

    print("Choose Downloader")
    if site == "pervcity":
        print("1 - Aria(best)  2 - tqdm(ok)")
    else:
        print("1 - Aria(best)  2 - tqdm(ok)  3 - pySmartDL(good)")
    while True:
        choice = input("Enter Choice: ")
        print("")
        if choice == "1":
            downloader = "aria"
            break
        elif choice == "2":
            downloader = "tqdm"
            break
        elif choice == "3" and site != "pervcity":
            downloader = "pySmartDL"
            break
        else:
            print("Invalid Choice!, Try Again", "\n")


def get_r(url, cookies=None, headers=None):
    r = requests.get(url, cookies=cookies, headers=headers)
    if r.status_code == 200:
        return r
    elif r.status_code == 400:
        print("Bad request: Check your request syntax or parameters.")
        return 0
    elif r.status_code == 401:
        print("Unauthorized: Invalid credentials.")
        return 0
    elif r.status_code == 403:
        print("Forbidden: User does not have permission to access the resource.")
        return 0
    elif r.status_code == 404:
        print("Not found: Login endpoint or resource not found.")
        return 0


evilmodel = None
fname = None


def get_filename(url):
    global evilmodel
    global fname
    if (
        site == "analonly"
        or site == "trueanal"
        or site == "allanal"
        or site == "dirtyauditions"
        or site == "nympho"
        or site == "swallowed"
    ):
        fname = url.split("?")[0].split("/")[10]
    elif site == "bangbros":
        fname = url.split("?")[0].split("/")[7]
    elif site == "pervcity":
        fname = url.split("/")[-1]
    elif site == "newsensations":
        fname = fname
    elif site == "evilangel":
        fname = evilmodel
    else:
        r = requests.head(url, cookies=cookies, allow_redirects=True)
        try:
            cd = r.headers["content-disposition"]
        except KeyError:
            return None
        if not cd:
            return None
        f = re.findall("filename=(.+)", cd)
        if len(f) == 0:
            return None
        fname = f[0]
        if (fname[0] == fname[-1]) and fname.startswith(("'", '"')):
            fname = fname[1:-1]
    return fname


def check_video(fname):
    cmd = "libs/ffprobe.exe " + fname
    cmd = shlex.split(cmd)
    a = subprocess.run(cmd, stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    return a.returncode


with open("evil.txt", "r") as file:
    evilfiles = [line.strip() for line in file if line.strip()]


def download(url):
    global downloader
    global dloc
    fname = get_filename(url)
    pname = fname
    fname = dloc + site.capitalize() + "/" + fname
    fpath = Path(fname)
    dname = os.path.dirname(fname)
    length = requests.head(url, cookies=cookies, allow_redirects=True).headers[
        "Content-Length"
    ]
    if int(length) < 1048576:
        print("File Size Too Small!")
        return 1
    if not os.path.isdir(dname):
        try:
            os.makedirs(os.path.expanduser(dname))
        except OSError as error:
            print(error)
    if Path.exists(fpath):
        if site == "pervcity":
            if str(check_video(fname)) == "1":
                print("Corrupt file, Downloading Again")
            elif str(check_video(fname)) == "0":
                print("File " + os.path.basename(fname) + " exists: Skipping.\n")
                return 0
            else:
                print("Invalid Data")
                return 0
        else:
            length = requests.head(url, cookies=cookies, allow_redirects=True).headers[
                "Content-Length"
            ]
            fsize = Path(fname).stat().st_size
            if int(length) <= fsize:
                print("File " + os.path.basename(fname) + " exists: Skipping.\n")
                return 0

    print("Downloading ", get_filename(url))
    if downloader == "aria":
        if "m3u8" in url:
            cmd = f'libs/youtube-dl.exe "{url}" -o "{fname.replace(" ", "_")}" --ffmpeg-location libs/ffmpeg.exe -q'
            print("m3u8 file: will be slow download")
        else:
            cmd = (
                f'libs/youtube-dl.exe --no-check-certificate --cookies "{cfile}" '
                f'-o "{fname.replace(" ", "_")}" "{url}" '
                f"--external-downloader libs/aria2c.exe --external-downloader-args "
                f'"-x 16 -s 16 -k 1M --file-allocation=falloc --check-certificate=false --console-log-level=error"'
            )
        cmd = shlex.split(cmd)
        subprocess.run(cmd)
        os.rename(fname.replace(" ", "_"), fname)

    elif downloader == "pySmartDL":
        temp = f"temp/"
        if not os.path.isdir(temp):
            try:
                os.makedirs(os.path.expanduser(temp))
            except OSError as error:
                print(error)
        obj = SmartDL(url, dest=temp)
        obj.start()
        ptime = obj.get_eta()
        if int(ptime) > 60:
            print("ETA:", round(int(ptime) / 60, 2), "min")
        else:
            print("ETA:", int(ptime), "sec")
        shutil.move(temp + os.path.basename(fname), dname)

    elif downloader == "tqdm":
        chunk_size = 1024
        dl = requests.get(url, cookies=cookies, stream=True, allow_redirects=True)
        if site == "pervcity":
            with open(fname, "wb") as fout:
                with tqdm(
                    unit="B",
                    unit_scale=True,
                    unit_divisor=1024,
                    miniters=1,
                    desc=os.path.basename(fname),
                ) as pbar:
                    for chunk in dl.iter_content(chunk_size=chunk_size):
                        fout.write(chunk)
                        pbar.update(len(chunk))
        else:
            with open(fname, "wb") as fout:
                with tqdm(
                    unit="B",
                    unit_scale=True,
                    unit_divisor=1024,
                    miniters=1,
                    desc=os.path.basename(fname),
                    total=int(dl.headers.get("content-length")),
                ) as pbar:
                    for chunk in dl.iter_content(chunk_size=chunk_size):
                        fout.write(chunk)
                        pbar.update(len(chunk))
    return 0


def getsize(url):
    global tsize
    r = requests.head(url, cookies=cookies, allow_redirects=True)
    length = r.headers["Content-Length"]
    return int(length)


def downloadlink(url):
    a = []
    global f
    global tsize
    global quality
    global cookies
    global headers
    if (
        site == "analonly"
        or site == "trueanal"
        or site == "allanal"
        or site == "dirtyauditions"
        or site == "nympho"
        or site == "swallowed"
    ):
        url = "https://members." + site + ".com" + url
        if not site == "dirtyauditions":
            with open(site + ".txt", "r") as file:
                urls = [line.strip() for line in file if line.strip()]
            if not url in urls:
                return 1
        for i in range(0, 3):
            r = requests.get(url, cookies=cookies, headers=headers)
            soup = BeautifulSoup(r.content, "html5lib")
            script = soup.find("script", {"type": "application/json"}).contents[0]
            data = json.loads(script.text)
            purl = data["props"]["pageProps"]["content"]["videos"][quality]["url"]
            if requests.head(purl).status_code == 200:
                url = purl
                break

    elif site == "bangbros":
        url = "https://members.bangbros.com" + url
        try:
            r = requests.get(url, cookies=cookies, headers=headers)
        except:
            print("Connection Error, Skipping Scene")
            return 1
        soup = BeautifulSoup(r.content, "html5lib")
        for link in soup.find_all("a", href=True):
            if quality + ".mp4" in link["href"]:
                a = 1
                url = link["href"]
            elif quality == "2160p" and "1080p.mp4" in link["href"] and a != 1:
                a = 1
                url = link["href"]
            elif quality == "1080p" and "720p.mp4" in link["href"] and a != 1:
                a = 1
                url = link["href"]
            elif quality == "720p" and "480p.mp4" in link["href"] and a != 1:
                url = link["href"]

    elif site == "newsensations":
        global fname
        url = "https://newsensations.com/members/" + url
        try:
            r = requests.get(url, cookies=cookies, headers=headers)
        except:
            print("Connection Error, Skipping Scene")
            return 1
        soup = BeautifulSoup(r.content, "html5lib")
        fname = soup.title.string + ".mp4" if soup.title else None
        urls = []
        for link in soup.find_all("a", href=True):
            if quality in link["href"]:
                a = 1
                urls.append(link["href"])
            elif (
                quality == "1920x1080MP4_12000"
                and "MP41920HD" in link["href"]
                and a != 1
            ):
                urls.append(link["href"])
            elif (
                quality == "1920x1080MP4_12000"
                and "MP41440x1080HD" in link["href"]
                and a != 1
            ):
                urls.append(link["href"])
            elif (
                quality in ["1920x1080MP4_12000", "1280x720MP4_6000"]
                and "853MP4_12000" in link["href"]
                and a != 1
            ):
                urls.append(link["href"])
        if urls:
            url = sorted(
                urls,
                key=lambda x: (not "MP41920HD" in x, "_1440" in x, "mike_adriano" in x),
            )[0]

    elif site == "pervcity":
        try:
            r = requests.get(url, cookies=cookies, headers=headers)
        except:
            print("Connection Error, Skipping Scene")
            return 1
        soup = BeautifulSoup(r.content, "html5lib")
        title = soup.find("title").text
        if title == "New Device Activation":
            print("New Device or Location Detected!", "\n")
            return 1
        data = soup.find_all("a", {"href": "javascript:void(0)"})
        for i in range(0, 12):
            if quality in str(data[i]).split('"')[3]:
                a = 1
                url = "https://members.pervcity.com/" + str(data[i]).split('"')[3]
                break
            elif (
                quality == "2160p"
                and "mp412000" in str(data[i]).split('"')[3]
                and a != 1
            ):
                url = "https://members.pervcity.com/" + str(data[i]).split('"')[3]
                break

    elif site == "evilangel":
        headers = {"X-Requested-With": "XMLHttpRequest"}
        url = "https://members.evilangel.com/media/streamingUrls/" + url
        try:
            r = get_r(url, cookies=cookies, headers=headers)
        except KeyboardInterrupt:
            print("Keyboard interrupt detected. Exiting gracefully.")
        except:
            print("Connection Error, Skipping Page")
            return 1
        soup = BeautifulSoup(r.content, "html.parser")
        data = json.loads(r.text)
        for purl in data:
            if quality == purl["format"]:
                a = 1
                url = purl["url"]
                break
            elif quality == "1080p" and "720p" == purl["format"] and a != 1:
                url = purl["url"]

    if want == "download":
        try:
            download(url)
        except:
            print("Download Error")
    elif want == "print":
        print(url, "\n")
    elif want == "fprint":
        print(get_filename(url), " copied!")
        print(url, file=f)


def getmodelnum(model):
    s = []
    if site == "bangbros":
        model = model.split(" , ")
        for models in model:
            url = "https://members.bluepillmen.com/search?models=" + models.replace(
                " ", "+"
            )
            r = requests.get(url, cookies=cookies, headers=headers)
            soup = BeautifulSoup(r.content, "html5lib")
            s.append(
                int(
                    soup.find_all("div", class_="vdo")[0]
                    .text.splitlines()[1]
                    .split(' ""')[0]
                )
            )
        return s


def getmodels(model):
    a = []
    if (
        site == "analonly"
        or site == "trueanal"
        or site == "allanal"
        or site == "dirtyauditions"
        or site == "nympho"
        or site == "swallowed"
    ):
        url = "https://members." + site + ".com/models/" + model.replace(" ", "-")
        r = requests.get(url, cookies=cookies, headers=headers)
        if r.status_code == 500:
            print("Invalid Model Name")
            exit(1)
        soup = BeautifulSoup(r.content, "html5lib")
        s = []
        for link in soup.find_all("a", href=True):
            if "/scenes/" in link["href"] and link["href"] != a:
                a = link["href"]
                s.append(link["href"])
        if site == "trueanal":
            s = s[:-2]
        for i in range(len(s)):
            downloadlink(s[i])

    elif site == "bangbros":
        url = (
            "https://members.bluepillmen.com/search?models=mike+adriano|"
            + model.replace(" , ", "|").replace(" ", "+")
        )
        r = requests.get(url, cookies=cookies, headers=headers)
        soup = BeautifulSoup(r.content, "html5lib")
        s = int(
            soup.find_all("div", class_="vdo")[0].text.splitlines()[1].split(' ""')[0]
        )
        if s >= 10437 or s == 0 or ("," in model and s in getmodelnum(model)):
            print("Invalid Model Name")
            exit(1)
        a = []
        for link in soup.find_all("a", href=True):
            if "/movie/" in link["href"] and link["href"] != a:
                a = link["href"]
                downloadlink(link["href"])

    elif site == "pervcity":
        url = (
            "https://members.pervcity.com/search.php?st=advanced&qall=mike+adriano+%2C+"
            + model.replace(",", "%2C").replace(" ", "+")
        )
        r = requests.get(url, cookies=cookies, headers=headers)
        if r.text == "Page not found":
            print("Invalid Model Name")
            exit(1)
        soup = BeautifulSoup(r.content, "html5lib")
        for link in soup.find_all("a", href=True):
            if "/scenes/" in link["href"] and link["href"] != a:
                a = link["href"]
                downloadlink(link["href"])

    elif site == "evilangel":
        headers = {
            "X-Algolia-API-Key": getapi(),
            "X-Algolia-Application-Id": "TSMKFA364Q",
            "Referer": "https://members.evilangel.com/",
        }
        payload = {
            "requests": [
                {
                    "indexName": "all_scenes_latest_desc",
                    "params": "query=" + model + " mike adriano",
                },
            ]
        }
        url = "https://tsmkfa364q-dsn.algolia.net/1/indexes/*/queries"
        r = requests.post(url, headers=headers, json=payload)
        data = json.loads(r.text)
        global evilmodel
        for url in data["results"][0]["hits"]:
            # print('https://members.evilangel.com/en/video/evilangel/' + url['title'].replace(' ', '-') + '/' + str(url  ['clip_id']))
            evilmodel = (
                ", ".join(
                    j["name"] for j in url["actors"] if j["name"] != "Mike Adriano"
                )
                + " - "
                + str(url["_highlightResult"]["movie_title"]["value"])
                + ".mp4"
            )
            downloadlink(str(url["clip_id"]))


def getlinks(i):
    if (
        site == "analonly"
        or site == "trueanal"
        or site == "allanal"
        or site == "dirtyauditions"
        or site == "nympho"
        or site == "swallowed"
    ):
        url = "https://members." + site + ".com/scenes?page=" + i
        key = "/scenes/"
    elif site == "bangbros":
        url = "https://members.bluepillmen.com/model/60703/latest/" + i
        key = "/movie/"
    elif site == "newsensations":
        url = "https://newsensations.com/members/sets.php?id=2360"
        key = "gallery"
    elif site == "pervcity":
        url = "https://members.pervcity.com/sets.php?id=551&page=" + i
        key = "/scenes/"
    try:
        r = get_r(url, cookies=cookies, headers=headers)
    except KeyboardInterrupt:
        print("Keyboard interrupt detected. Exiting gracefully.")
    except:
        print("Connection Error, Skipping Page")
        return 1
    soup = BeautifulSoup(r.content, "html5lib")
    a = []
    urls = []
    for link in soup.find_all("a", href=True):
        if key in link["href"] and link["href"] != a:
            a = link["href"]
            urls.append(link["href"])
            time.sleep(0.001)
    if site == "trueanal":
        urls = urls[:-2]
    if not urls:
        print("either pages end reached or some error")
        exit(0)
    for url in urls:
        downloadlink(url)


def getapi():
    url = "https://members.evilangel.com/"
    headers = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64)"}
    r = requests.get(url, cookies=cookies, headers=headers)
    soup = BeautifulSoup(r.content, "html.parser")
    apikey = soup.select("script")[2].string.split('"apiKey":"')[1].split('"}')[0]
    return apikey


def getevilangel(i):
    headers = {
        "X-Algolia-API-Key": getapi(),
        "X-Algolia-Application-Id": "TSMKFA364Q",
        "Referer": "https://members.evilangel.com/",
    }
    payload = {
        "requests": [
            {
                "indexName": "all_scenes_latest_desc",
                "params": "query=&hitsPerPage=60&maxValuesPerFacet=1000&page="
                + i
                + '&analytics=true&analyticsTags=["component:searchlisting","section:members","site:evilangel","context:videos","device:desktop"]&attributesToRetrieve=["isVR","video_formats","clip_id","title","url_title","pictures","categories","actors","release_date","sitename","clip_length","upcoming","network_name","length","ratings_up","ratings_down","rating_rank","clip_path","channels","mainChannel","views","award_winning","directors","trailers","subtitles","objectID","subtitle_id","source_clip_id","hasPpu","ppu_infos","action_tags"]&highlightPreTag=<ais-highlight-0000000000>&highlightPostTag=</ais-highlight-0000000000>&facetingAfterDistinct=true&clickAnalytics=true&filters=(content_tags:\'straight\' OR content_tags:\'lesbian\')&facets=["directors.name","serie_name","categories.name","actors.name","video_formats.format","availableOnSite","categories.name","directors.name","upcoming"]&tagFilters=&facetFilters=["categories.name:-EA Short","directors.name:Mike Adriano",["upcoming:0"],["directors.name:Mike Adriano"],["categories.name:-EA Short"]]'
                # "params": "query=Mike Adriano -EA&hitsPerPage=60&maxValuesPerFacet=1000&page=" + i
            },
        ]
    }
    url = "https://tsmkfa364q-dsn.algolia.net/1/indexes/*/queries"
    r = requests.post(url, headers=headers, json=payload)
    data = json.loads(r.text)
    global evilmodel
    for url in data["results"][0]["hits"]:
        evilmodel = (
            ", ".join(j["name"] for j in url["actors"] if j["name"] != "Mike Adriano")
            + " - "
            + str(url["_highlightResult"]["movie_title"]["value"])
            + ".mp4"
        )
        downloadlink(str(url["clip_id"]))


if method == "siterip":
    x = 1
    y = 100
else:
    x = 1
    y = 2

if site != "newsensations" and method != "models" and method != "siterip":
    x = int(input("Enter Starting Page: "))
    y = int(input("Enter Ending Page: "))
    while True:
        if x > y:
            print("Invalid Number, Enter Again")
        else:
            if x == 0:
                x += 1
            y += 1
            break

tsize = 0

if method == "pages" or method == "siterip":
    for i in range(x, y):
        print("page", i)
        if site == "evilangel":
            getevilangel(str(i - 1))
        else:
            getlinks(str(i))

if method == "models":
    getmodels(model)

if want == "fprint":
    f.close()
